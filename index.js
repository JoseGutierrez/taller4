import express from "express";
import connectDB from "./src/configs/mongo.js";
import { PORT } from "./src/configs/environments.js";
import authRouter from "./src/routes/auth.router.js";
import userRouter from "./src/routes/user.router.js";
import messageRouter from "./src/routes/message.router.js";
const app = express();
app.use(express.json());

app.use("/auth", authRouter);
app.use("/users", userRouter);
app.use("/messages", messageRouter);

app.get("/", (req, res) => {
  res.json({ student: "Jose Gutierrez." });
});

async function startServer() {
  const isConnected = await connectDB();
  if (isConnected) {
    app.listen(PORT, () => {
      console.log(`Server started on ${PORT}`);
    });
  } else {
    process.exit();
  }
}

startServer();
