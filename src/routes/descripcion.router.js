import express from "express";
import descripcionService from "../services/descripcion.service.js";
const router = express.Router();
const service = new descripcionService();

router.get("/", async (req, res) => {
  res.status(200).json(await service.descripcion());
});

export default router;
