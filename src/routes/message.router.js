import express from "express";
import {
  sendMessage,
  deleteMessage,
} from "../controllers/message.controller.js";

const router = express.Router();

router.post("/", sendMessage);
router.delete("/:messageId", deleteMessage);

export default router;
