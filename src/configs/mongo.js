import mongoose from "mongoose";
import { MONGO_URI } from "./environments.js";

export default function conectDB() {
  return mongoose
    .connect(MONGO_URI)
    .then((success) => {
      console.log("MongoDB connected successfully");
      registerModels();
      return true;
    })
    .catch((error) => {
      console.log("MongoDB not conected");
      return false;
    });
}

async function registerModels() {
  // await import("../models/message.model.js");
  await import("../models/user.model.js");
}
