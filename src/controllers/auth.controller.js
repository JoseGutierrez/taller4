import User from "../models/user.model.js";
import bcrypt from "bcrypt";
export async function register(req, res) {
  const body = req.body;
  try {
    const password = req.body.password;
    const encryptedPassword = bcrypt.hashSync(password, 10);
    const newUser = new User({
      name: body.name,
      email: body.email,
      dni: body.dni,
      password: encryptedPassword,
    });
    await newUser.save();
    return res.status(201).send({
      message: "User Registered Successfully",
    });
  } catch (error) {
    console.log(error);
    return res.status(500).send({
      message: "User Registration Failed",
    });
  }
}

export async function login(req, res) {
  const body = req.body;
  try {
    const users = await User.find();
    const userLogin = users.find((user) => user.email == body.email);
    if (userLogin) {
      const password = req.body.password;
      const isMatch = await bcrypt.compare(password, userLogin.password);
      console.log(password);
      if (isMatch) {
        return res.status(200).send({
          message: "User Encontrado",
        });
      } else {
        return res.status(401).send({
          message: "Invalid Credentials",
        });
      }
    } else {
      return res.status(404).send({
        message: "User Not Found",
      });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).send({
      message: "Error Server",
    });
  }
}
