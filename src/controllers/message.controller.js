import Message from "../models/message.model.js";

export async function getMessage(req, res) {
  const message = await Message.find();
  return res.status(200).send({ message });
}

export async function getMessages(req, res) {
  try {
    const message = await Message.find({ userId: req.params.userId });
    return res.status(200).send({ message });
  } catch (error) {
    return res.status(500).send({ error });
  }
}
export async function deleteMessage(req, res) {
  const messagetId = req.params.messageId;
  console.log(messagetId);
  const me = await Message.find();
  const message = await me.findById(messagetId);

  if (!message) {
    return res.status(404).send({ error: "No encontrado" });
  }

  await product.delete();

  return res.status(204).send();
}

export async function sendMessage(req, res) {
  const body = req.body;
  try {
    const message = await Message.create(body);
    return res.status(201).send({ message });
  } catch (error) {
    return res.status(500).send({
      message: "Message Not Send",
    });
  }
}
